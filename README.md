[![Codacy Badge](https://api.codacy.com/project/badge/Grade/f68e00a6d689406e99ba41108daeb0fd)](https://www.codacy.com/app/alistairmcmillan75/JSMinesweeper?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=alistairmcmillan/JSMinesweeper&amp;utm_campaign=Badge_Grade)

JSMinesweeper
=============

Minesweeper written in JavaScript.

Started as a quick nighttime hack to fight insomnia.

Ideal for employees of "Enterprise" companies that block or remove games from their PC.

Intended to be as close as possible to Minesweeper as found on Windows XP.

Playable version available here [http://mcmillan.cx/minesweeper/](http://mcmillan.cx/minesweeper/).
